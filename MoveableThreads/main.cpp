#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void background_work(int id, int delay)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;
        this_thread::sleep_for(chrono::milliseconds(delay));
    }

    cout << "END OF WORK THD#" << id << endl;
}

thread create_thread(int id)
{
    thread thd(&background_work, id, 200);

    return move(thd);
}

int main()
{
    cout << "Main thread starts..." << endl;

    thread thd1 = create_thread(1);
    cout << "id of thd1: " << thd1.get_id() << endl;
    thread mvd_thd1 = move(thd1);
    auto thd2 = create_thread(2);

    cout << "id of thd1 po move: " << thd1.get_id() << endl;
    cout << "id of mvd_thd1: " << mvd_thd1.get_id() << endl;
    cout << "id of thd2: " << thd2.get_id() << endl;

    // group of threads
    vector<thread> thread_group(4);

    thread_group[0] = move(mvd_thd1);
    thread_group[1] = move(thd2);
    thread_group[2] = create_thread(3);
    thread_group[3] = thread(&background_work, 4, 500);
    thread_group.push_back(thread(&background_work, 5, 500));
    thread_group.emplace_back(&background_work, 6, 100);

    for(auto& thd : thread_group)
        thd.join();

    cout << "Main thread end..." << endl;
}
