#include <iostream>
#include <thread>
#include <chrono>
#include <cassert>
#include <vector>
#include <iterator>
#include <functional>

using namespace std;

void hello(const string& text)
{
    for(auto c : text)
    {
        cout << c << " ";
        //this_thread::yield();
        this_thread::sleep_for(chrono::milliseconds(100));
        cout.flush();
    }

    cout << endl;
}

class BackgroundTask
{
    const int id_;
public:
    BackgroundTask(int id) : id_{id}
    {}

    void operator()(int ms)
    {
        for(int i = 0; i < 10; ++i)
        {
            cout << "BT#" << id_ << ": " << i << endl;
            this_thread::sleep_for(chrono::milliseconds(ms));
        }

        cout << "BT#" << id_ << " is finished..." << endl;
    }
};

class Bitmap
{
    vector<char> buffer_;
public:
    void assign(const vector<char>& source)
    {
        buffer_.assign(source.begin(), source.end());
    }

    const std::vector<char>& data() const
    {
        return buffer_;
    }
};

int main()
{
   vector<int> v = {10};

    copy(v.begin(), v.end(), ostream_iterator<int>(cout, " "));

    cout << "Main thread starts..." << endl;

    thread thd1(&hello, "Hello thread...");
    thread thd2(&hello, "Hello concurrent...");

    BackgroundTask bt1{1};
    thread thd3{bt1, 200};

    thread thd4{BackgroundTask(2), 700};

    string text =  "Thread with lambda...";

    thread thd5{[=]{ hello(text); }};

     thd1.join();
    thd2.join();
    thd3.join();
    thd4.detach();
    assert(!thd4.joinable());

    thd5.join();

    cout << "\n\n---------------------------" << endl;

    vector<char> data = { 'b', 'i', 't', 'm', 'a', 'p' };

    Bitmap bmp1;
    Bitmap bmp2;

    thread cpy_thd1{bind(&Bitmap::assign, ref(bmp1), cref(data))};
    thread cpy_thd2{[&bmp2, &data]{bmp2.assign(data);}};

    cpy_thd1.join();
    cpy_thd2.join();

    cout << "Bitmap1: ";
    for(auto c : bmp1.data())
        cout << c;
    cout << endl;

    cout << "Bitmap2: ";
    for(auto c : bmp2.data())
        cout << c;
    cout << endl;

    cout << "Main thread ends..." << endl;
}
