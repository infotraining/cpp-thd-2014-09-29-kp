#include <iostream>
#include <queue>
#include <algorithm>
#include <thread>
#include <atomic>
#include "thread_safe_queue.hpp"

using namespace std;

typedef string Data;

const Data end_of_work = "";

class ProducerConsumer
{
    ThreadSafeQueue<Data> data_queue_;
    int counter_;
public:
    ProducerConsumer(int counter) : counter_{counter}
    {}

    void produce(Data data)
    {
        while(next_permutation(data.begin(), data.end()))
        {
            cout << "Producing data: " << data << endl;
            data_queue_.push(data);
            this_thread::sleep_for(chrono::milliseconds(25));
        }

        for(int i = 0; i < counter_; ++i)
            data_queue_.push(end_of_work);
    }

    void consume(int id)
    {
        while (true)
        {
            Data data;

            data_queue_.pop(data); // waits for data

            if (data == end_of_work)
                return;

            process_data(id, data);
        }
    }
private:
    void process_data(int id, const string& data)
    {
        cout << "Consumer#" << id << " is processing " << data << endl;
        this_thread::sleep_for(chrono::milliseconds(100));
    }
};


int main()
{
    ProducerConsumer pc{2};

    thread producer{ [&pc] { pc.produce("abcde"); }};
    thread consumer1{[&pc] { pc.consume(1); }};
    thread consumer2{[&pc] { pc.consume(2); }};

    producer.join();
    consumer1.join();
    consumer2.join();
}
