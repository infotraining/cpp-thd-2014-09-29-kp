#include <iostream>
#include <chrono>
#include <thread>
#include <future>
#include "thread_safe_queue.hpp"

using namespace std;

void background_task(int id)
{
    cout << "BT#" << id << " starts..." << endl;

    this_thread::sleep_for(chrono::milliseconds(rand() % 5000));

    cout << "BT#" << id << " ends..." << endl;
}

typedef function<void()> Task;

class ThreadPool
{
public:
    ThreadPool(size_t no_of_threads) : threads_(no_of_threads)
    {
        for(size_t i = 0; i < no_of_threads; ++i)
            threads_[i] = thread{[this] { run(); }};
    }

    ThreadPool(const ThreadPool& ) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            task_queue_.push(END_OF_WORK);

        for(auto& t : threads_)
            t.join();
    }

//    void submit(Task t)
//    {
//        if (t == END_OF_WORK)
//            throw std::logic_error("You can't submit nullptr");

//        task_queue_.push(t);
//    }

    template <typename F>
    auto submit(F f) -> future<decltype(f())>
    {
        typedef typename result_of<F()>::type result_type;

        auto task = make_shared<packaged_task<result_type()>>(f);
        future<result_type> fresult = task->get_future();
        task_queue_.push([task] { (*task)(); });

        return fresult;
    }

private:
    ThreadSafeQueue<Task> task_queue_;
    vector<thread> threads_;
    const static nullptr_t END_OF_WORK;

    void run()
    {
        while(true)
        {
            Task task;
            task_queue_.pop(task);

            if (task == END_OF_WORK)
                return;

            task();
        }
    }
};

int calculate_square(int x)
{
    cout << "Starting calculation for: " << x << endl;

    int interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    //may_throw(x);

    return x * x;
}

int main()
{
    ThreadPool pool{8};

    pool.submit([] { background_task(1); });
    pool.submit([] { background_task(2); });

    for(int i = 3; i <= 20; ++i)
        pool.submit([=] { background_task(i);});

    vector<future<int>> squares;

    for(int i = 0; i < 20; ++i)
        squares.push_back(pool.submit([=] { return calculate_square(i);}));

    cout << "\n\nSquares: ";
    for(auto& s : squares)
        cout << s.get() << " ";
    cout << endl;
}
