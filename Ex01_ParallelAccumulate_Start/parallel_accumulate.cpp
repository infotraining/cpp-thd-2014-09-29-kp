#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>

template <typename It, typename T>
void accumulate_by_ref(It first, It last, T& result)
{
    result = std::accumulate(first, last, T());
}

template <typename It, typename T>
T parallel_accumulate(It first, It last, T init)
{
    unsigned int hardware_threads_count = std::max(std::thread::hardware_concurrency(), 1u);

    std::vector<std::thread> thds(hardware_threads_count-1); // grupa watków roboczych
    std::vector<T> partial_results(hardware_threads_count);

    size_t count = std::distance(first, last);
    size_t range_size = count / hardware_threads_count;


    It range_start = first;
    for(size_t i = 0; i < hardware_threads_count - 1; ++i)
    {
        It range_end = range_start;
        std::advance(range_end, range_size);

        thds[i] = std::thread{&accumulate_by_ref<It, T>,
                              range_start, range_end, std::ref(partial_results[i])};

        range_start = range_end;
    }

    accumulate_by_ref(range_start, last, partial_results.back());

    for(auto& t : thds)
        t.join();

    return std::accumulate(partial_results.begin(), partial_results.end(), init);
}


int main(int argc, char *argv[])
{
    const size_t SIZE = 50000000;

    typedef unsigned long long int ValueType;

    std::vector<ValueType> v(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    {
        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     std::accumulate(v.begin(), v.end(), ValueType());
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }

    {
        std::cout << "\nParallel\n";

        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     parallel_accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }
}
