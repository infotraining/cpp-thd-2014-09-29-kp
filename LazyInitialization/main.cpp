#include <iostream>
#include <memory>
#include <mutex>
#include <atomic>

using namespace std;

class Service
{
public:
    virtual void run() = 0;
    virtual ~Service() {}
};

class RealService : public Service
{
    string url_;
    mutex mtx_;
public:
    RealService(const string& url) : url_{url}
    {
        cout << "Creating a real service..." << endl;
        cout << "Open port for real service: " << url_ << endl;
    }

    void run() override
    {
        lock_guard<mutex> lk(mtx_);
        cout << "RealService::run() - " << url_ << endl;
    }
};

namespace atomics_impl
{
    class ProxyService : public Service
    {
        atomic<RealService*> real_service_;
        string url_;
        mutex mtx_;
    public:
        ProxyService(const string& url) : real_service_{nullptr}, url_{url}
        {}

        ProxyService(const ProxyService&) = delete;
        ProxyService& operator=(const ProxyService&) = delete;

        ~ProxyService()
        {
            delete real_service_.load();
        }

        void run() override
        {
            if (real_service_.load() == nullptr)
            {
                lock_guard<mutex> lk{mtx_};

                // lazy initializtion
                if (real_service_.load() == nullptr)
                {
                    RealService* temp = new RealService{url_};
                    real_service_ .store(temp);
                }
            }

            (*real_service_).run();
        }
    };
}

namespace call_once_impl
{
    // VirtualProxy
    class ProxyService : public Service
    {
        mutex mtx_;
        unique_ptr<RealService> real_service_;
        string url_;
        once_flag flag_;
    public:
        ProxyService(const string& url) : url_(url)
        {
            cout << "Creating lightweight proxy for service..." << endl;
        }

        void run() override
        {
            call_once(flag_, [this] { real_service_.reset(new RealService(url_)); });

            real_service_->run();
        }
    };
}

class Client
{
    shared_ptr<Service> service_;
public:
    Client(shared_ptr<Service> service) : service_{service}
    {}

    void do_something()
    {
        service_->run();
    }
};

int main()
{
    shared_ptr<Service> srv1 = make_shared<call_once_impl::ProxyService>("http://77.33.44.55");

    cout << "\n\n";

    Client client(srv1);
    client.do_something();
}
