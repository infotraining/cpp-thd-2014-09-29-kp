#include <iostream>
#include <thread>
#include <chrono>
#include <atomic>
#include <mutex>

using namespace std;

const int max_iterations = 100000000;

// shared variable


int counter;
mutex mtx;

//atomic<int> counter{0};

void worker()
{
    for(int i = 0; i < max_iterations; ++i)
    {
        mtx.lock();

        lock_guard<mutex> lk(mtx, adopt_lock);
        counter++;
    }
}

int main()
{
    auto start = chrono::high_resolution_clock::now();

    thread thd1{&worker};
    thread thd2{&worker};

    thd1.join();
    thd2.join();

    auto end = chrono::high_resolution_clock::now();

    float interval_ms =
            chrono::duration_cast<chrono::milliseconds>(end-start).count();

    cout << "Time: " << interval_ms << "ms" << endl;
    cout << "Counter: " << counter << endl;
}
