#include <iostream>
#include <future>
#include <utility>

using namespace std;

typedef std::string FileContent;

FileContent download_file(const string& url)
{
    cout << "Starting download file: " << url << endl;
    auto interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    return "Content of file " + url;
}

shared_future<FileContent> download_file_async(const string& url)
{
    shared_future<FileContent> result = async(launch::async, &download_file, url);

    return result;
}

void save_to_file(const string& filename, const string& content)
{
    cout << "Starting saving to file: " << filename << endl;
    auto interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    cout << "File " << filename << " saved with content " << content << endl;
}

template <typename Callable, typename... Args>
auto spawn_task(Callable callable, Args&&... args) -> future<decltype(callable(args...))>
{
    typedef decltype(callable(args...)) result_type;
    packaged_task<result_type()> task {[=] { return callable(forward<Args>(args)...); }};

    auto future_result = task.get_future();

    thread thd{move(task)};
    thd.detach();

    return future_result;
}


int main()
{
//    shared_future<FileContent> f1 = download_file_async("http://nokia.com");

//    for(int i = 0; i < 10; ++i)
//    {
//        cout << "Main thread works..." << endl;
//        this_thread::sleep_for(chrono::milliseconds(100));
//    }


//    shared_future<FileContent> f2 = f1;

//    auto f3 = f1;

//    cout << "f1: " << f1.get() << endl;
//    cout << "f2: " << f2.get() << endl;
//    cout << "f3: " << f3.get() << endl;

//    cout << "\n\n--------------------------" << endl;

//    {
//        auto s1 = async(launch::async, &save_to_file, "file1.txt", "Text1");
//        auto s2 = async(launch::async, &save_to_file, "file2.txt", "Text2");
//        auto s3 = async(launch::async, &save_to_file, "file3.txt", "Text3");

//        cout << "End of scope" << endl;
//    }

//    cout << "After scope" << endl;

    cout << "\n\n--------------------------" << endl;

    async(launch::async, &save_to_file, "file1.txt", "Text1");
    async(launch::async, &save_to_file, "file2.txt", "Text2");
    async(launch::async, &save_to_file, "file3.txt", "Text3");

    cout << "\n\n--------------------------" << endl;

    {
        spawn_task([]{ save_to_file("file1.txt", "Text1"); });
        spawn_task(&save_to_file, "file1.txt", "Text1" );
        spawn_task([]{ save_to_file("file1.txt", "Text1"); });
    }

    this_thread::sleep_for(chrono::seconds(10));
}
