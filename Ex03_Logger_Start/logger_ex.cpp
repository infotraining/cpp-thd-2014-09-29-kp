#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <functional>
#include <string>
#include <mutex>
#include <future>
#include "thread_safe_queue.hpp"

using namespace std;

namespace Before
{

class Logger
{
    ofstream fout_;
    mutex mtx_;
public:
    Logger(const string& file_name)
    {
        fout_.open(file_name);
    }

    ~Logger()
    {
        fout_.close();
    }

    void log(const string& message)
    {
        lock_guard<mutex> lk{mtx_};

        fout_ << message << endl;
        fout_.flush();
    }
};

}

namespace After
{
    typedef function<void()> Task;

    class ActiveObject
    {
    public:
        ActiveObject() : is_done_{false}, thread_{&ActiveObject::run, this}
        {
        }

        ActiveObject(const ActiveObject&) = delete;
        ActiveObject& operator=(const ActiveObject&) = delete;

        ~ActiveObject()
        {
            send([&] { is_done_ = true; });

            thread_.join();
        }

//        void send(Task task)
//        {
//            task_queue_.push(task);
//        }

        template <typename F>
        auto send(F f) -> future<decltype(f())>
        {
            typedef typename result_of<F()>::type result_type;

            auto task = make_shared<packaged_task<result_type()>>(f);
            future<result_type> fresult = task->get_future();
            task_queue_.push([task] { (*task)(); });

            return fresult;
        }

    private:
        ThreadSafeQueue<Task> task_queue_;
        thread thread_;
        bool is_done_;

        void run()
        {
            while (!is_done_)
            {
                Task task;
                task_queue_.pop(task);
                task();
            }
        }
    };

    class Logger
    {
        ofstream fout_;
        ActiveObject ao_;
    public:
        Logger(const string& file_name)
        {
            fout_.open(file_name);
        }

        ~Logger()
        {
            ao_.send([=] { fout_.close(); });
        }

        void log(const string& message)
        {
            ao_.send([message, this] { do_log(message);});
        }
    private:
        void do_log(const string& message)
        {
            fout_ << message << endl;
            fout_.flush();
        }
    };

    class Document
    {
        ActiveObject ao_;
    public:
        string load_from_file(const string& file_name)
        {
            return "Content";
        }

        future<string> load_from_file_async(const string& file_name)
        {
            return ao_.send([this, file_name] { return load_from_file(file_name);});
        }
    };
}

using namespace After;

void run(Logger& logger, int id)
{
    for(int i = 0; i < 1000; ++i)
        logger.log("Log#" + to_string(id) + " - Event#" +to_string(i));
}

int main()
{
    /*
     * Napisz klase Logger, ktora jest thread-safe
     */

    Logger log("data.log");

    thread thd1(&run, ref(log), 1);
    thread thd2(&run, ref(log), 2);

    thd1.join();
    thd2.join();
}
