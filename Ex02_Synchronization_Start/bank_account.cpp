#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    const int id_;
    double balance_;
    typedef std::recursive_mutex Mutex;

    mutable Mutex mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::lock_guard<Mutex> lk(mtx_);
        std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<Mutex> lk_from(mtx_, std::defer_lock);
        std::unique_lock<Mutex> lk_to(to.mtx_, std::defer_lock);

        std::lock(lk_from, lk_to);

        withdraw(amount);
        to.deposit(amount);
    }

    void withdraw(double amount)
    {
        std::lock_guard<Mutex> lk(mtx_);
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<Mutex> lk(mtx_);
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<Mutex> lk(mtx_);
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};



void make_withdraws(BankAccount& ba, int no_of_iters)
{
    for(int i = 0; i < no_of_iters; ++i)
        ba.withdraw(1.0);
}

void make_deposit(BankAccount& ba, int no_of_iters)
{
    for(int i = 0; i < no_of_iters; ++i)
        ba.deposit(1.0);
}

void make_transfers(BankAccount& from, BankAccount& to, int no_of_iters, int id)
{
    for(int i = 0; i < no_of_iters; ++i)
    {
        std::cout << "THD#" << id << " transfer from ba#" << from.id() << " to ba#" << to.id() << std::endl;
        from.transfer(to, 1.0);
    }
}

int main()
{
    using namespace std;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    const int NO_OF_ITERS = 100000;

    thread thd1{&make_withdraws, ref(ba1), NO_OF_ITERS};
    thread thd2{[&ba1] { make_deposit(ba1, NO_OF_ITERS); }};

    // possible deadlock
    thread thd3{&make_transfers, ref(ba1), ref(ba2), NO_OF_ITERS, 1};
    thread thd4{&make_transfers, ref(ba2), ref(ba1), NO_OF_ITERS, 2};

    thd1.join();
    thd2.join();

    thd3.join();
    thd4.join();

    cout << "\n\nAfter threads:\n";
    ba1.print();
    ba2.print();

    {
        lock_guard<BankAccount> lk(ba1);
        ba1.withdraw(900.0);
        ba1.withdraw(88.0);
    }
}
