#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

timed_mutex mtx;

void background_work(int id, int timeout)
{
    cout << "THD#" << id << " is waiting for mutex..." << endl;

    unique_lock<timed_mutex> lk(mtx, try_to_lock);

    if (!lk.owns_lock())
    {
        do
        {
            cout << "THD#" << id << " doesn't own lock..."
                 << " Tries to acuire mutex..." << endl;
        }
        while (!lk.try_lock_for(chrono::milliseconds(timeout)));
    }

    cout << "Start of THD#" << id << endl;

    this_thread::sleep_for(chrono::seconds(10));

    lk.unlock();
    cout << "End of work THD#" << id << endl;
}

int main()
{
    thread thd1{background_work, 1, 1000};
    thread thd2{background_work, 2, 750};

    thd1.join();
    thd2.join();
}
