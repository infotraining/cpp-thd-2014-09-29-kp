#include <iostream>
#include <thread>
#include <vector>
#include <string>

using namespace std;

void may_throw(int id, int arg)
{
    if (arg == 13)
        throw runtime_error("Error#13 from THD#"s + to_string(id));
}

void background_work(int id, size_t count, exception_ptr& excpt)
{
    try
    {
        for(int i = 0; i  < count; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;
            this_thread::sleep_for(100ms);

            may_throw(id, i);
        }

        cout << "THD#" << id << " has finished..." << endl;
    }
    catch(...)
    {
        excpt = current_exception();
    }
}

int main()
{
    cout << "Hardware concurrency: " << thread::hardware_concurrency() << endl;

    cout << "Main starts..." << endl;

    try
    {
        exception_ptr excpt1;
        thread thd1{&background_work, 1, 12, ref(excpt1)};

        exception_ptr excpt2;
        thread thd2{&background_work, 2, 16, ref(excpt2)};

        thd1.join();
        thd2.join();

        if (excpt1)
        {
            rethrow_exception(excpt1);
        }

        if (excpt2)
        {
            rethrow_exception(excpt2);
        }
    }
    catch(const runtime_error& e)
    {
        cout << "Caught exception: " << e.what() << endl;
    }

    cout << "\n\n---------------------------\n\n";

    const int no_of_threads = 16;

    vector<thread> thread_group(no_of_threads);
    vector<exception_ptr> expts(no_of_threads);

    // creation of threads
    for(int i = 0; i < no_of_threads; ++i)
        thread_group[i] = thread{&background_work, i+1, i + 12, ref(expts[i])};

    for(auto& thd : thread_group)
        thd.join();

    for(auto& e : expts)
    {
        if (e)
        {
            try
            {
                rethrow_exception(e);
            }
            catch(const runtime_error& e)
            {
                cout << "Main catch: " << e.what() << endl;
            }
        }
    }

    cout << "Main ends..." << endl;
}
