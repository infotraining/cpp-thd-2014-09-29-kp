#include <iostream>
#include <future>
#include <chrono>
#include <vector>
#include <functional>

using namespace std;

void may_throw(int x)
{
    if (x == 13)
        throw runtime_error("Error#13");
}

int calculate_square(int x)
{
    cout << "Starting calculation for: " << x << endl;

    int interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    may_throw(x);

    return x * x;
}

void save_to_file(const string& msg)
{
    cout << "Saving to file... " << msg << endl;

    int interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));
}

class SquareCalculator
{
    promise<int> promise_;

public:
    void operator()(int x)
    {
        int interval = rand() % 5000;

        this_thread::sleep_for(chrono::milliseconds(interval));

        try
        {
            may_throw(x);
        }
        catch(...)
        {
            exception_ptr excpt = current_exception();
            promise_.set_exception(excpt);
            return;
        }

        promise_.set_value(x * x);
    }

    future<int> get_future()
    {
        return promise_.get_future();
    }
};

int main()
{
//    // 1-szy sposób

//    packaged_task<int()> pt1{[] { return calculate_square(12); }};
//    packaged_task<int()> pt2{bind(&calculate_square, 9)};
//    packaged_task<int(int)> pt3{&calculate_square};
//    packaged_task<void(string)> pt4{&save_to_file};

//    future<int> f1 = pt1.get_future();
//    auto f2 = pt2.get_future();
//    auto f3 = pt3.get_future();
//    auto f4 = pt4.get_future();

//    pt1();  // wywolanie synchroniczne

//    thread thd1{move(pt2)}; // wywolanie asynchroniczne
//    thread thd2{move(pt3), 13};
//    thread thd3{move(pt4), "Text"};

//    try
//    {
//        cout << "result f1: " << f1.get() << endl;
//        cout << "result f2: " << f2.get() << endl;
//        cout << "result f3: " << f3.get() << endl;
//        f4.wait();
//    }
//    catch(const runtime_error& e)
//    {
//        cout << "Caught exception: " << e.what() << endl;
//    }

//    thd1.join();
//    thd2.join();
//    thd3.join();

//    cout << "\n\n----------------------" << endl;

//    // 2-gi sposób

//    SquareCalculator square_calc;

//    thread thd4{ref(square_calc), 78};

//    future<int> f5 = square_calc.get_future();

//    cout << "result f5: " << f5.get() << endl;
//    thd4.join();

    // 3. sposób

    cout << "\n\n----------------------" << endl;

    future<int> f6 = async(launch::async, &calculate_square, 67);

    cout << "async called..." << endl;

    this_thread::sleep_for(chrono::milliseconds(2000));

    cout << "result f6: " << f6.get() << endl;

    vector<future<int>> future_squares;


    for(int i = 0; i < 20; ++i)
        future_squares.push_back(async(launch::async, [=]{ return calculate_square(i);}));

    cout << "Async tasks started..." << endl;

    this_thread::sleep_for(chrono::milliseconds(3000));

    for(auto& f : future_squares)
    {
        try
        {
            cout << "Result: " << f.get() << endl;
        }
        catch(const runtime_error& e)
        {
            cout << "Exceprion caught: " << e.what() << endl;
        }
    }
}
